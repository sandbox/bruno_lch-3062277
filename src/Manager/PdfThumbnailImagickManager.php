<?php

namespace Drupal\pdf_thumbnail\Manager;

use Imagick;

/**
 * Class PdfThumbnailImagickManager.
 *
 * @package Drupal\pdf_thumbnail\Manager
 */
class PdfThumbnailImagickManager {

  /**
   * Generate image from PDF file.
   *
   * @param string $source
   *   File source.
   * @param string $target
   *   File target.
   *
   * @return string
   *   File path.
   *
   * @throws \ImagickException
   */
  public function generateImage($source, $target) {
    $target = dirname($source) . DIRECTORY_SEPARATOR . $target;
    $im = new Imagick($source . "[0]");
    $im->setimageformat("png");
    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
    $im->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
    $im->setImageBackgroundColor('white');
    $im->thumbnailimage(1240, 1754);
    $im->writeimage($target);
    $fileName = $im->getImageFilename();
    $im->clear();
    $im->destroy();
    return $this->moveImageToThumbnailFolder($fileName);
  }

  /**
   * Move thumbnail image file to thumbnail folder.
   *
   * @param string $filePath
   *   File path.
   *
   * @return string|null
   *   File image path.
   */
  protected function moveImageToThumbnailFolder($filePath) {
    if (file_exists($filePath)) {
      $filePathArray = explode('/', $filePath);
      if (!empty($filePathArray)) {
        $parentFolder = $filePathArray[count($filePathArray) - 2];
        $fileName = $filePathArray[count($filePathArray) - 1];
        if ($parentFolder) {
          $destination = 'public://styles/thumbnail/public/' . $parentFolder . '/';
          if (file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
            if (file_unmanaged_move($filePath, $destination,
              FILE_EXISTS_REPLACE)) {
              return $destination . $fileName;
            }
          }
        }
      }
    }
    return NULL;
  }

}
