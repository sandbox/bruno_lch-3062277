<?php

namespace Drupal\pdf_thumbnail\Manager;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystem;

/**
 * Class PdfThumbnailImageManager.
 *
 * @package Drupal\pdf_thumbnail\Manager
 */
class PdfThumbnailImageManager {

  /**
   * PdfThumbnailManager.
   *
   * @var \Drupal\pdf_thumbnail\Manager\PdfThumbnailImagickManager
   */
  protected $PdfThumbnailImagickManager;

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * FileSystem.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * PdfThumbnailManager constructor.
   *
   * @param \Drupal\pdf_thumbnail\Manager\PdfThumbnailImagickManager $PdfThumbnailImagickManager
   *   $PdfThumbnailImagickManager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   EntityTypeManager.
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   FileSystem.
   */
  public function __construct(
    PdfThumbnailImagickManager $PdfThumbnailImagickManager,
    EntityTypeManager $entityTypeManager,
    FileSystem $fileSystem
  ) {
    $this->PdfThumbnailImagickManager = $PdfThumbnailImagickManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * Create pdf thumbnail.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \ImagickException
   */
  public function createThumbnail(EntityInterface $entity) {
    $fieldDefinitions = $entity->getFieldDefinitions();
    if ($fieldDefinitions) {
      foreach ($fieldDefinitions as $fieldDefinition) {
        if ($fieldDefinition->getType() == "file") {
          if (!empty($entity->get($fieldDefinition->getName())->getValue())) {
            foreach ($entity->get($fieldDefinition->getName())->getValue() as $fieldValue) {
              if (array_key_exists('target_id', $fieldValue)) {
                $tid = $fieldValue['target_id'];
                $fileEntity = $this->entityTypeManager->getStorage('file')
                  ->load($tid);
                if ($fileEntity->getMimeType() == 'application/pdf') {
                  $fileUri = $fileEntity->getFileUri();
                  $fileUriArray = explode('/', $fileUri);
                  $fileName = end($fileUriArray);
                  $sourcePath = $this->fileSystem->realpath($fileUri);
                  if ($sourcePath) {
                    $imageFileUri = $this->PdfThumbnailImagickManager->generateImage($sourcePath,
                      $fileName . '.png');
                    $fileEntityId = $this->createThumbnailFileEntity($imageFileUri);
                    if ($fileEntityId) {
                      $entity->set('field_thumbnail', ['target_id' => $fileEntityId]);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * Create file entity.
   *
   * @param string $fileUri
   *   File uri.
   *
   * @return int|null|string
   *   File entity id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createThumbnailFileEntity($fileUri) {
    if ($fileUri) {
      $fileEntity = $this->entityTypeManager->getStorage('file')->create([
        'uri' => $fileUri,
        'status' => FILE_STATUS_PERMANENT,
      ]);
      $fileEntity->save();
      return $fileEntity->id();
    }
    return NULL;
  }

}
