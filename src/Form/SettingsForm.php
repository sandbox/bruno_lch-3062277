<?php

namespace Drupal\pdf_thumbnail\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Regenerate thumbnails'),
      '#description' => $this->t('Regenrate thumbnail images.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $batch = [
      'title' => t('Regenerates thumbnails'),
      'operations' => [
        [
          '\Drupal\pdf_thumbnail\Form\settingsForm::regenerateThumbnails',
          [],
        ],
      ],
    ];
    batch_set($batch);
  }

  /**
   * RegenerateThumbnails.
   */
  public function regenerateThumbnails() {
    $res = Drupal::entityTypeManager()->getStorage('node')->getQuery()
      ->condition('bundle', 'document_privat')
      ->execute();
    if ($res) {
      $medias = Drupal::entityTypeManager()->getStorage('node')
        ->loadMultiple($res);
      if ($medias) {
        foreach ($medias as $media) {
          $media->save();
        }
      }
      Drupal::messenger()
        ->addMessage(t('Images have been regenerated'), 'success');
    }
  }

}
